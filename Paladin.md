# Paladin
20200604204034

#armpgy #rpg-class

### Specs

* Defensive frontline
    * Shield wearer
    * Supports allies
    * Heal adjacent by damaging foes
    * Protects allies
* Aggressive flanker
    * Two handed weapons
    * AoE fire/holy aura
    * Big swing radius, FF?
    * Maybe some ranged excorsim
* Ranged support 
    * Holy aura to prevent undead from raising within it's reach
        * Many could have the aura, Paladins version gives each affected their own aura so instead of the Paladin emitting the aura at long range he emits and aura that makes the affected member emits an aura
    * Healing range same as aura range
    * Long casting time for heals but maybe increased by this spec