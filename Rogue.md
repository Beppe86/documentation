# Rogue
20210109101147

#armpgy #rpg-class

### Specs
* Stealth
    * Sneaks slowly around invisible trying to find good spots for attack
    * Can reveal and attack for bonus damage, preferable when not too many enemies around
* Combo
    * Can enter state with increadibly high movement speed and fast attacks as long as each attack is on a new opponent (low damage attacks)
    * Stealth as emergency retreat
    * Failed attack cancels the combo state
    * Tactics could be to set behaviour to only attack new target if hit chance is higher than 80%
* Debuffer
    * Poision
    * Immobolize
    * Stun
    * Blind