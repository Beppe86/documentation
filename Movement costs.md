# Movement costs
20200729145747

See also:
* [Pathfinder](Pathfinder.md)

#armpgy #armpgy-system

### Armour affect movement cost
In order to give more meaning to a 'pathfinder' class which can find even ground the movement penalty in hilly terrain for heavy armour should be quite severe. Moving downhill should give lighter units an energy boost but for a heavy armoured unit it might even cost more to walk downhill than it would just walking on flat ground. Uphill should be more costly for most units but the size of the penalty could depend on armour and perks.