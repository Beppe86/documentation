# Bomber
20210109114715

#armpgy #rpg-class

### Equiptment
* Gasmask

### Spec
* Long range thrower
    * Small explosions
    * Sets on fire
* Short range
    * Proximity mine
    * Remote controlled mine
    * Timed mine
    * Extra big bombs less frequently
* Multi thrower
    * Throws 3 grenades at a time
    * Gas bomb
