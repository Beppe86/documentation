# Combat system
20210116105300

See also:
[Movement costs](Movement costs.md)


#armpgy #armpgy-system

# Attributes

I want to work with high integers to improve precision without introducing floats, I can always round them ingame if needed.
Examples:
* Beginner character might start 1000Hp

Gain a passive 'power' trait from reaching 100, diminishing returns or more expensive after 100 (Inspired from Wizardry 8)
Stamina should be similar to mana as a resource, ignoring it will give chars a lot of downtime unless supported - speccing for Stamina should not also mean the char gets a lot of HP aka Constitution cannot be the only controlling factor for both

Use cases that should be easy for player to figure out how to solve

* My character hits hard but gets exhausted too quickly, what stats should I improve?
* I want more mana regen
* I want my spells to do more damage
* I want my auras to reach further

# Stats

* I want to include
    * Strenght
        * PP : Harder to be force moved
        * PP : Easier to move others
    * Agility
        * PP: Avoidance
    * Intelligence
        * PP: Spell penetration
    * Wisdom
    * Constitution/Vitality
        * PP: Resistance to conditions
* I will include some of these
    * Dexterity
    * Charisma
    * Speed

# Defence
I like the solution implemented by World of Warcraft
Attackers attack rating + random roll determines the success of the attack based on a hit table created by the targets defensive capabilities.

Dodge might require character to switch hexagon, might not be possible in formation

### Hit table
The higher the difference is between the attacks rating and the defensive raiting the higher up on this hit table the attack will become.
The idea is that tanklike characters should be able to become immune to critical hits and maybe even ordinary hits. I am a bit hesitant to using critical hits since the game should ideally be wearing the player down instead of punishing players for one misstep.
There will also be stamina drain based on this table for both attackers and defenders - TBD

* Critical hit
* Hit
* Blocked - Blocked attacks will still hurt defender
* Parrying
* Glancing - TBD
* Dodge
* Miss

Check posibility of each scenario individually

* Miss is only up to attacker, attack skill vs the difficulty of the attack
* Dodge could be up to enivronment, more open hexes means bigger chance of dodge
* Glancing is only up to defenders, might be affected by armor types
* Parrying could be an evaluation between attacker and defenders weapon skills, this phase could decide counters
* Blocking is only decided by defender

Percentage of all of these outcomes are ADDED together and produces the final chance to hit

### Armor
Categorizing armor into Light/Medium/Heavy might be to constricting, not sure yet.
Armor value on item
Not too restrictive on who can wear what to encourage more unique classes, instead a character should rather want to wear items with less armor for other bonuses such items give

# Magic
Direct target spells should be slightly underwhelming rather than overwhelming, melee/ranged char should be able to deal out more damage than a spell user.
Spells excel at manipulate terrain, zone control, buff and debuff

benchmarks for different spells

* Direct target spell
* Direct healing spell
* Healing over time
* Damage over time
* Zone control
* Hexagons on fire should be very punishing to stay in

# Initiative
Characters have a counter (TimeToAct) that counts down to zero which is when they get to act again.
Different actions will increase the TimeToAct value by different amounts
Speed should affect amount of ticks, not ability costs